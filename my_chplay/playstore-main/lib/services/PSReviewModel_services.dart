import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:playstore_flutter/model/PSModel.dart';

Future<String> _loadGameAsset() async {
  return await rootBundle.loadString('assets/Games.json');
}
Future LoadModel() async {
  String jsonString = await _loadGameAsset();
  final jsonResponse = json.decode(jsonString);
  PSReviews psReviews = new PSReviews.fromJson(jsonResponse);
}