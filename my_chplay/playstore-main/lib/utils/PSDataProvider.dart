import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:playstore_flutter/model/PSModel.dart';
import 'package:playstore_flutter/utils/PSConstants.dart';
import 'package:playstore_flutter/utils/PSImages.dart';



List<PSReviews> getReviewList() {
  List<PSReviews> list1 = [];
  list1.add(PSReviews(cirLogo: "N", title: "Nguyen Van An", date: "11/10/21", subTile: "Toi rat thich tua game nay"));
  list1.add(PSReviews(cirLogo: "R", title: "Nguyen Xuan Hoang", date: "11/1/222", subTile: "game hay"));
  list1.add(PSReviews(
      cirLogo: "B",
      title: "Blue Legend",
      date: "11/28/20",
      subTile: "tuyet voi "));
  return list1;
}

List<CategoriesApps> getCategoriesList() {
  List<CategoriesApps> categories = [];

  categories.add(CategoriesApps(name: 'Action', icon: Icons.star));
  categories.add(CategoriesApps(name: 'Adventure', icon: Icons.data_usage_rounded));
  categories.add(CategoriesApps(name: 'Arcade', icon: (Icons.add_comment)));
  categories.add(CategoriesApps(name: 'Board', icon: (Icons.add_circle_outline)));
  categories.add(CategoriesApps(name: 'Card', icon: Icons.credit_card));
  categories.add(CategoriesApps(name: 'Casino', icon: Icons.casino_outlined));
  categories.add(CategoriesApps(name: 'Eduction', icon: Icons.cast));
  categories.add(CategoriesApps(name: 'Music', icon: Icons.music_note_rounded));
  categories.add(CategoriesApps(name: 'Puzzle', icon: Icons.padding));
  categories.add(CategoriesApps(name: 'Racing', icon: Icons.pages));
  categories.add(CategoriesApps(name: 'Role Playing', icon: Icons.local_play_outlined));
  categories.add(CategoriesApps(name: 'Simulation', icon: Icons.circle_notifications));
  categories.add(CategoriesApps(name: 'Sports', icon: Icons.sports));
  categories.add(CategoriesApps(name: 'Strategy', icon: Icons.amp_stories_rounded));
  categories.add(CategoriesApps(name: 'Trivia', icon: Icons.trip_origin));
  categories.add(CategoriesApps(name: 'Word', icon: Icons.work_outline_sharp));
  return categories;
}

List<CategoriesApps> getCategoriesListApp() {
  List<CategoriesApps> categoriesapps = [];

  categoriesapps.add(CategoriesApps(name: 'Art & Design', icon: Icons.design_services));
  categoriesapps.add(CategoriesApps(name: 'Augmented reality', icon: Icons.data_usage_rounded));
  categoriesapps.add(CategoriesApps(name: 'Auto & Vehicles', icon: (Icons.date_range)));
  categoriesapps.add(CategoriesApps(name: 'Beauty', icon: (Icons.add_circle_outline)));
  categoriesapps.add(CategoriesApps(name: 'Books & Reference', icon: Icons.credit_card));
  categoriesapps.add(CategoriesApps(name: 'Beauty', icon: Icons.casino_outlined));
  categoriesapps.add(CategoriesApps(name: 'Books & Reference', icon: Icons.cast));
  categoriesapps.add(CategoriesApps(name: 'Business', icon: Icons.business));
  categoriesapps.add(CategoriesApps(name: 'Comics', icon: Icons.padding));
  categoriesapps.add(CategoriesApps(name: 'Dating', icon: Icons.pages));
  categoriesapps.add(CategoriesApps(name: 'Eduction', icon: Icons.local_play_outlined));
  categoriesapps.add(CategoriesApps(name: 'Events', icon: Icons.circle_notifications));
  categoriesapps.add(CategoriesApps(name: 'Finance', icon: Icons.sports));
  categoriesapps.add(CategoriesApps(name: 'Food & Drink', icon: Icons.amp_stories_rounded));
  categoriesapps.add(CategoriesApps(name: 'Sports', icon: Icons.trip_origin));
  categoriesapps.add(CategoriesApps(name: 'Games', icon: Icons.work_outline_sharp));

  return categoriesapps;
}

List<ReviewModel> getReviewList1() {
  List<ReviewModel> reviewList = [];
  reviewList.add(ReviewModel(img: 'images/cloneApp/playStore/ps_playstore.png', name: 'Nguyen Minh Chau', date: '21/11/2020', review: Review, rating: 2.0));
  reviewList.add(ReviewModel(img: 'images/cloneApp/playStore/ps_playstore.png', name: 'Minh Han', date: '11/10/2020', review: review1, rating: 4.0));
  reviewList.add(ReviewModel(img: 'images/cloneApp/playStore/ps_playstore.png', name: 'Anh Tuan', date: '15/02/2020', review: Review, rating: 1.0));
  reviewList.add(ReviewModel(img: 'images/cloneApp/playStore/ps_playstore.png', name: 'Thuy Duong', date: '25/08/2020', review: Review, rating: 5.0));
  reviewList.add(ReviewModel(img: 'images/cloneApp/playStore/ps_playstore.png', name: 'Thao Nhi', date: '16/06/2020', review: review1, rating: 2.0));
  return reviewList;
}

List<GameModelList> getGameListGame() {
  List<GameModelList> gameList = [];
  gameList.add(GameModelList(img: PS_GameImg1, videoImg: 'assets/images/videoIcon.gif'));
  gameList.add(GameModelList(img: PS_GameImg2));
  gameList.add(GameModelList(img: PS_GameImg3));
  gameList.add(GameModelList(img: PS_GameImg4));
  gameList.add(GameModelList(img: PS_GameImg5));
  gameList.add(GameModelList(img: PS_GameImg6));
  gameList.add(GameModelList(img: PS_GameImg7));
  gameList.add(GameModelList(img: PS_GameImg8));

  return gameList;
}

List<GameModelList> getGameListGame1() {
  List<GameModelList> gameList1 = [];
  gameList1.add(GameModelList(img: PS_GameImg1));
  gameList1.add(GameModelList(img: PS_GameImg2));
  gameList1.add(GameModelList(img: PS_GameImg3));
  gameList1.add(GameModelList(img: PS_GameImg4));
  gameList1.add(GameModelList(img: PS_GameImg5));
  gameList1.add(GameModelList(img: PS_GameImg6));
  gameList1.add(GameModelList(img: PS_GameImg7));
  gameList1.add(GameModelList(img: PS_GameImg8));

  return gameList1;
}

List<GameModelList> getGameListGame2() {
  List<GameModelList> gameList2 = [];
  gameList2.add(GameModelList(img: PS_GameImg1));
  gameList2.add(GameModelList(img: PS_GameImg2));
  gameList2.add(GameModelList(img: PS_GameImg3));
  gameList2.add(GameModelList(img: PS_GameImg4));
  gameList2.add(GameModelList(img: PS_GameImg5));
  gameList2.add(GameModelList(img: PS_GameImg6));
  gameList2.add(GameModelList(img: PS_GameImg7));
  gameList2.add(GameModelList(img: PS_GameImg8));

  return gameList2;
}

List<GameModelList> getGameListGame3() {
  List<GameModelList> gameList3 = [];
  gameList3.add(GameModelList(img: PS_GameImg1));
  gameList3.add(GameModelList(img: PS_GameImg2));
  gameList3.add(GameModelList(img: PS_GameImg3));
  gameList3.add(GameModelList(img: PS_GameImg4));
  gameList3.add(GameModelList(img: PS_GameImg5));
  gameList3.add(GameModelList(img: PS_GameImg6));
  gameList3.add(GameModelList(img: PS_GameImg7));
  gameList3.add(GameModelList(img: PS_GameImg8));
  return gameList3;
}
