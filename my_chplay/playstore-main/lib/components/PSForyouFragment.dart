import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:playstore_flutter/model/PSAppbarModel.dart';
import 'package:playstore_flutter/screens/PSDetailScreen.dart';
import 'package:playstore_flutter/utils/AppWidget.dart';
import 'package:playstore_flutter/utils/PSColor.dart';

class PSForyouFragment extends StatefulWidget {
  static String tag = '/TopCharts';
  final int tabIndex;

  PSForyouFragment(this.tabIndex);

  @override
  PSForyouFragmentState createState() => PSForyouFragmentState();
}

class PSForyouFragmentState extends State<PSForyouFragment> {
  int number = 1;
  bool isInstall = false;
  dynamic data;

  final Stream<QuerySnapshot> _dataStream = FirebaseFirestore.instance.collection('PSGameModel')
      .snapshots();

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _dataStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          var list1 = snapshot.data?.docs.toList();
          return Column(
              children: [
                16.height,
                Row(
                  children: [
                    16.width,
                    commonCacheImageWidget(
                        snapshot.data?.docs[3]['imgLogo'], height: 50,
                        width: 50,
                        fit: BoxFit.cover).cornerRadiusWithClipRRect(10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(snapshot.data?.docs[3]['title'],
                            style: boldTextStyle(size: 12),
                            overflow: TextOverflow.ellipsis),
                        Text(snapshot.data?.docs[3]['subTitle'],
                            style: secondaryTextStyle(size: 12)),
                        Row(
                          children: [
                            Text((snapshot.data?.docs[3]['rating']).toString(),
                                style: secondaryTextStyle(size: 12)),
                            Icon(Icons.star, size: 10),
                            4.width,
                            Icon(Icons.add_box_rounded, size: 16),
                          ],
                        ),
                      ],
                    ).onTap(() {
                      PSDetailScreen(index: 3).launch(context);
                    }).paddingOnly(left: 8).expand(),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.grey[300]!)),
                      height: 30,
                      width: 70,
                      child: Text(
                      snapshot.data?.docs[3]['install'] ? 'CanCel' : 'Install',
                      style: secondaryTextStyle(color: psColorGreen),
                      ).center().onTap(() {PSDetailScreen(index: 3).launch(context);}),
                    ).paddingOnly(right: 24)
                  ],
                ),
              Container(
                padding: EdgeInsets.all(16),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Banner (
                    message: 'Hot game',
                    location: BannerLocation.topEnd,
                    color: Colors.red,
                    child: Container(
                      height: 186,
                      width: 280,
                      child: Image.network(snapshot.data?.docs[3]['imgLogo'],
                        fit: BoxFit.cover,
                      ),
                    ).cornerRadiusWithClipRRect(10),
                  ).onTap(() {
          PSDetailScreen(index: 3).launch(context);
          }),
                ),
              ).cornerRadiusWithClipRRect(10),
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.data?.docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        child: Column(
                          children: [
                            16.height,
                            Row(
                              children: [
                                8.width,
                                commonCacheImageWidget(list1?[index]['imgLogo'], height: 50, width: 60, fit: BoxFit.cover).cornerRadiusWithClipRRect(10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(snapshot.data?.docs[index]['title'], style: boldTextStyle()).paddingOnly(left: 16),
                                    Text(snapshot.data?.docs[index]['subTitle'], style: secondaryTextStyle()).paddingOnly(left: 16),
                                    Row(
                                      children: [
                                        Text((snapshot.data?.docs[index]['rating']).toString(), style: secondaryTextStyle()).paddingOnly(left: 16),
                                        Icon(Icons.star, size: 10),
                                        Text((snapshot.data?.docs[index]['appSize']).toString()).paddingOnly(left: 16),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ).onTap(() {
                              PSDetailScreen(index: index).launch(context);
                            })
                          ],
                        ).paddingOnly(left: 16));
                  })
            ],
          );
        });
  }
}
