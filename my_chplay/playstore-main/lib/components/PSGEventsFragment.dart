import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:playstore_flutter/model/PSAppbarModel.dart';
import 'package:playstore_flutter/screens/PSEventDetailScreen.dart';
import 'package:playstore_flutter/utils/AppWidget.dart';
import 'package:playstore_flutter/utils/PSColor.dart';

class PSGEventsFragment extends StatefulWidget {
  static String tag = '/Gevents';

  final int tabIndex;

  PSGEventsFragment(this.tabIndex);

  @override
  PSGEventsFragmentState createState() => PSGEventsFragmentState();
}

class PSGEventsFragmentState extends State<PSGEventsFragment> {
  bool button = false;
  List<PSAppbarModel> data = getGameList;
  var events = getGameList[2].list;

  final Stream<QuerySnapshot> _dataStream = FirebaseFirestore.instance.collection('Event')
      .snapshots();
  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _dataStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          return ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: snapshot.data?.docs.length,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  PSEventDetailScreen(index: index).launch(context);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    16.height,
                    Row(
                      children: [
                        commonCacheImageWidget(
                            snapshot.data?.docs[index]['imgLogo'], height: 50,
                            width: 50,
                            fit: BoxFit.cover).cornerRadiusWithClipRRect(10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(snapshot.data?.docs[index]['title'],
                                style: boldTextStyle(size: 12),
                                overflow: TextOverflow.ellipsis),
                            Text(snapshot.data?.docs[index]['subTitle'],
                                style: secondaryTextStyle(size: 12)),
                            Row(
                              children: [
                                Text((snapshot.data?.docs[index]['rating']).toString(),
                                    style: secondaryTextStyle(size: 12)),
                                Icon(Icons.star, size: 10),
                                4.width,
                                Icon(Icons.add_box_rounded, size: 16),
                              ],
                            ),
                          ],
                        ).paddingOnly(left: 8).expand(),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.grey[300]!)),
                          height: 30,
                          width: 70,
                          child: Text(
                            snapshot.data?.docs[index]['install'] ? 'CanCel' : 'Install',
                            style: secondaryTextStyle(color: psColorGreen),
                          ).center().onTap(() {
                            setState(() {
                              if (!events![index].install!) {
                                events![index].install = true;
                              } else {
                                events![index].install = false;
                              }
                            });
                          }),
                        ).paddingOnly(right: 24)
                      ],
                    ),
                    16.height,
                    Row(
                      children: [
                        Icon(Icons.event, size: 10),
                        4.width,
                        Text(snapshot.data?.docs[index]['event'], style: secondaryTextStyle(
                            size: 12)),
                        4.width,
                        Text(snapshot.data?.docs[index]['ends'], style: secondaryTextStyle(
                            size: 12)),
                      ],
                    ),
                    8.height,
                    Text(snapshot.data?.docs[index]['subTitle1'], style: secondaryTextStyle())
                        .paddingOnly(right: 16),
                    16.height,
                    commonCacheImageWidget(snapshot.data?.docs[index]['imgMain'], height: 180,
                        width: context.width(),
                        fit: BoxFit.cover)
                        .cornerRadiusWithClipRRect(10)
                        .paddingOnly(right: 16),
                    16.height,
                  ],
                ).paddingOnly(left: 16),
              );
            },
          );
        });
  }
}
