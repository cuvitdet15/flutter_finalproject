import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:playstore_flutter/model/PSAppbarModel.dart';
import 'package:playstore_flutter/screens/PSTopChartDetailScreen.dart';
import 'package:playstore_flutter/utils/AppWidget.dart';

class PSTopChartsFragment extends StatefulWidget {
  static String tag = '/TopCharts';
  final int tabIndex;

  PSTopChartsFragment(this.tabIndex);

  @override
  PSTopChartsFragmentState createState() => PSTopChartsFragmentState();
}

class PSTopChartsFragmentState extends State<PSTopChartsFragment> {
  var list = getGameList[1].categories![0].list;
  int number = 1;
  bool isInstall = false;
  dynamic data;

  final Stream<QuerySnapshot> _dataStream = FirebaseFirestore.instance.collection('RelationApp')
      .snapshots();

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _dataStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
      if (snapshot.hasError) {
        return Text('Something went wrong');
      }

      if (snapshot.connectionState == ConnectionState.waiting) {
        return Text("Loading");
      }
      var list1 = snapshot.data?.docs.toList();
    return Column(
      children: [
             ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: snapshot.data?.docs.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      child: Column(
                    children: [
                      16.height,
                      Row(
                        children: [
                          8.width,
                          commonCacheImageWidget(list1?[index]['imgLogo'], height: 50, width: 60, fit: BoxFit.cover).cornerRadiusWithClipRRect(10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(snapshot.data?.docs[index]['title'], style: boldTextStyle()).paddingOnly(left: 16),
                              Text(snapshot.data?.docs[index]['subTitle'], style: secondaryTextStyle()).paddingOnly(left: 16),
                              Row(
                                children: [
                                  Text((snapshot.data?.docs[index]['rating']).toString(), style: secondaryTextStyle()).paddingOnly(left: 16),
                                  Icon(Icons.star, size: 10),
                                  Text((snapshot.data?.docs[index]['appSize']).toString()).paddingOnly(left: 16),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ).onTap(() {
                        PSTopChartDetailScreen(index: index).launch(context);
                      })
                    ],
                  ).paddingOnly(left: 16));
                })
      ],
    );
        });
  }
}
