import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:playstore_flutter/screens/PSDashboardScreen.dart';
import 'package:playstore_flutter/store/AppStore.dart';
import 'package:playstore_flutter/utils/AppTheme.dart';
import 'package:playstore_flutter/utils/PSConstants.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

AppStore appStore = AppStore();
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //await initialize();
  appStore.toggleDarkMode(value: await getBool(isDarkModeOnPref, defaultValue: false));
  runApp(
    Phoenix(child: MyApp())

  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return MaterialApp(
          title: 'PlayStore flutter',
          theme: !appStore.isDarkModeOn ? AppThemeData.lightTheme : AppThemeData.darkTheme,
          builder: scrollBehaviour(),
          debugShowCheckedModeBanner: false,
          home: PSDashboardScreen(),
        );
      },
    );
  }
}


