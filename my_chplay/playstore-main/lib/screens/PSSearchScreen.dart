import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:playstore_flutter/screens/PSDetailScreen.dart';
import 'package:playstore_flutter/utils/AppWidget.dart';

class PSSearchScreen extends StatefulWidget {
  static String tag = '/TopCharts';
  final String title;

  PSSearchScreen({required this.title});

  @override
  PSSearchScreenState createState() => PSSearchScreenState();
}

class PSSearchScreenState extends State<PSSearchScreen> {
  final Stream<QuerySnapshot> _dataStream = FirebaseFirestore.instance.collection('PSGameModel')
      .snapshots();

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _dataStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          var list1 = snapshot.data?.docs.toList();
          return Scaffold(
              appBar: AppBar(
              // backgroundColor: Colors.white,
              //elevation: 0,
              title: Text(widget.title, style: boldTextStyle()),
          ),
            body: Column(
            children: [
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.data?.docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        child: Column(
                          children: [
                            16.height,
                            Row(
                              children: [
                                8.width,
                                commonCacheImageWidget(list1?[index]['imgLogo'], height: 50, width: 60, fit: BoxFit.cover).cornerRadiusWithClipRRect(10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(snapshot.data?.docs[index]['title'], style: boldTextStyle()).paddingOnly(left: 16),
                                    Text(snapshot.data?.docs[index]['subTitle'], style: secondaryTextStyle()).paddingOnly(left: 16),
                                    Row(
                                      children: [
                                        Text((snapshot.data?.docs[index]['rating']).toString(), style: secondaryTextStyle()).paddingOnly(left: 16),
                                        Icon(Icons.star, size: 10),
                                        Text((snapshot.data?.docs[index]['appSize']).toString()).paddingOnly(left: 16),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ).onTap(() {
                              PSDetailScreen(index: index).launch(context);
                            })
                          ],
                        ).paddingOnly(left: 16));
                  })
            ],
          )
          );
        });
  }
}
