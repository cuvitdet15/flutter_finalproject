import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:playstore_flutter/components/PSAppsForYouComponent.dart';
import 'package:playstore_flutter/components/PSTopChartsFragment.dart';
import 'package:playstore_flutter/components/PSGEditorChoiceFragment.dart';
import 'package:playstore_flutter/model/PSAppbarModel.dart';
import 'package:playstore_flutter/model/PSModel.dart';
import 'package:playstore_flutter/screens/PSGameViewAllScreen.dart';
import 'package:playstore_flutter/utils/PSColor.dart';
import 'package:playstore_flutter/utils/PSWidgets.dart';
import '../utils/PSDataProvider.dart';

class PSAppsScreen extends StatefulWidget {
  static String tag = '/PSAppsScreen';
  final List<PSAppbarModel> list = getGameList;

  @override
  PSAppsScreenState createState() => PSAppsScreenState();
}

class PSAppsScreenState extends State<PSAppsScreen> with TickerProviderStateMixin {
  List<PSAppbarModel> list = appsList;
  List<CategoriesApps> categoriesList = getCategoriesListApp();

  TabController? _tabController;
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    _tabController = TabController(vsync: this, initialIndex: tabIndex, length: appsList.length);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController?.dispose();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: context.height(),
      padding: EdgeInsets.only(top: 8),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 50,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey[300]!))),
                child: TabBar(
                  unselectedLabelColor: Colors.black54,
                  controller: _tabController,
                  indicatorColor: Colors.green,
                  labelColor: psColorGreen,
                  indicatorSize: TabBarIndicatorSize.label,
                  labelStyle: boldTextStyle(size: 12),
                  isScrollable: true,
                  tabs: appsList.map((e) {
                    return Tab(text: e.name.validate());
                  }).toList(),
                  onTap: (i) {
                    tabIndex = i;
                    setState(() {});
                  },
                ),
              ),
            ),
            forYouList(context, tabIndex, list, categoriesList),
          ],
        ),
      ),
    );
  }
}

Widget forYouList(BuildContext context, int tabIndex, List<PSAppbarModel> list, List<CategoriesApps> categoriesList) {
  if (tabIndex == 0) {
    return PSAppsForyouFragment(tabIndex).paddingBottom(16);
  } else if (tabIndex == 1) {
    return PSTopChartsFragment(tabIndex).paddingBottom(16);
  } else if (tabIndex == 2) {
    return CategoriesList(data: categoriesList);
  }else if (tabIndex == 3) {
    return PSGEditorChoiceFragment(tabIndex);
  }
    return SizedBox();
  }
